# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='ContributerModel',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('date_created', models.DateTimeField(null=True, auto_created=True, blank=True)),
                ('name', models.CharField(max_length=100)),
                ('description', models.CharField(max_length=150)),
            ],
            options={
                'ordering': ('date_created',),
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='CourseModel',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('date_created', models.DateTimeField(null=True, auto_created=True, blank=True)),
                ('name', models.CharField(max_length=50)),
                ('description', models.CharField(max_length=150)),
            ],
            options={
                'ordering': ('date_created',),
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='LessonModel',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('date_created', models.DateTimeField(null=True, auto_created=True, blank=True)),
                ('name', models.CharField(max_length=100)),
                ('courses', models.ManyToManyField(to='main_bot_app.CourseModel')),
            ],
            options={
                'ordering': ('date_created',),
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='TelegramUserModel',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('date_created', models.DateTimeField(null=True, auto_created=True, blank=True)),
                ('name', models.CharField(max_length=100)),
                ('last_lesson_id', models.IntegerField(default=0)),
                ('last_word_id', models.IntegerField(default=0)),
                ('courses', models.ForeignKey(to='main_bot_app.CourseModel')),
            ],
            options={
                'ordering': ('date_created',),
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='WordModel',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('date_created', models.DateTimeField(null=True, auto_created=True, blank=True)),
                ('word', models.CharField(max_length=100)),
                ('pronounce', models.CharField(max_length=150, null=True, blank=True)),
                ('synonym_eng', models.TextField(null=True, blank=True)),
                ('antonym_eng', models.TextField(null=True, blank=True)),
                ('meaning_fa', models.TextField()),
                ('examples', models.TextField()),
            ],
            options={
                'ordering': ('date_created',),
            },
            bases=(models.Model,),
        ),
        migrations.AddField(
            model_name='lessonmodel',
            name='words',
            field=models.ManyToManyField(to='main_bot_app.WordModel'),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='contributermodel',
            name='courses',
            field=models.ManyToManyField(to='main_bot_app.CourseModel'),
            preserve_default=True,
        ),
    ]
