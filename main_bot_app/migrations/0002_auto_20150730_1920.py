# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('main_bot_app', '0001_initial'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='telegramusermodel',
            name='courses',
        ),
        migrations.AddField(
            model_name='telegramusermodel',
            name='course',
            field=models.IntegerField(null=True, blank=True),
            preserve_default=True,
        ),
    ]
