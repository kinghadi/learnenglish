# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('main_bot_app', '0002_auto_20150730_1920'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='telegramusermodel',
            name='last_lesson_id',
        ),
        migrations.RemoveField(
            model_name='telegramusermodel',
            name='last_word_id',
        ),
        migrations.AddField(
            model_name='telegramusermodel',
            name='last_lesson',
            field=models.IntegerField(default=-1, null=True, blank=True),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='telegramusermodel',
            name='course',
            field=models.IntegerField(default=-1, null=True, blank=True),
            preserve_default=True,
        ),
    ]
