# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('main_bot_app', '0003_auto_20150730_1935'),
    ]

    operations = [
        migrations.AddField(
            model_name='lessonmodel',
            name='lesson_number',
            field=models.IntegerField(default=1),
            preserve_default=True,
        ),
    ]
