# -*- coding: utf-8 -*-
from __future__ import unicode_literals
import json
import urllib2, urllib
from django.http.response import JsonResponse, HttpResponse
from django.views.decorators.csrf import csrf_exempt
import emoji
from main_bot_app.models import CourseModel, TelegramUserModel, LessonModel

BASE_URL = "https://api.telegram.org/bot74911736:AAGupWAZcxm32lusxvv9QI_n-P7MwOz3wEc/"
import json

REGISTER_COMMAND = '/register'
HELP_COMMAND = '/help'
LESSON_COMMAND = '/lesson'
STOP_COMMAND = '/stop'
state = ''

COURSE_504 = 'کتاب ۵۰۴'


def getme_view(request):
    text = json.load(urllib2.urlopen(BASE_URL + 'getMe'))
    return JsonResponse(text)


def getupdate_view(request):
    text = json.load(urllib2.urlopen(BASE_URL + 'getUpdates'))
    return JsonResponse(text)


def set_webhook_view(request):
    url = "https://murmuring-chamber-7482.herokuapp.com/webhook"
    text = url
    if url:
        a = json.load(urllib2.urlopen(BASE_URL + 'setWebhook', urllib.urlencode({'url': url})))
        text = dict(a)
    return JsonResponse(text)


def register_course():
    all_objects = CourseModel.objects.all()
    all_courses = [c.name for i, c in enumerate(all_objects)]
    reply_markup = {'keyboard': [[c.name] for c in all_objects], 'resize_keyboard': True, 'one_time_keyboard': True}
    return '\n'.join(all_courses), json.dumps(reply_markup)


def next_word(uid):
    if TelegramUserModel.objects.filter(name=uid).exists():
        user = TelegramUserModel.objects.filter(name=uid)
        if user.courses.count() > 0:
            lesson = LessonModel.objects.all()[user.last_lesson_id]
            total_words = lesson.words.count()

            # check to see if it is the new first time
            if total_words > user.last_word_id:
                word = lesson.words.all()[user.last_word_id]
                user.last_word_id += 1
                user.save()
                msg = word.word
            else:
                msg = 'تعداد لغات این درس به اتمام رسیده. از /lesson برای رفتن به درس جدید استفاده کنید.'
        else:
            msg = 'ابتدا ثبت نام کنید. از دستور /register استفاده کنید'
    else:
        msg = 'ابتدا ثبت نام کنید. از دستور /register استفاده کنید'
    return msg


def create_lesson(words):
    text = ''
    for word in words:
        text += word.word
        text += '\n'
    return text


def create_header(lesson_name, course_name):
    header = '#{} -> #{}'.format(course_name.replace(' ', ''), lesson_name)
    header += '\n'
    header += '=' * 10
    header += '\n'
    return header


def create_footer():
    footer = 'در صورت تمایل به همکاری در ایجاد محتوا به آی دی ' + "@hadi7070" + " پیام بدهید!"
    return '=' * 10 + '\n' + footer


def get_lesson(uid):
    try:
        user = TelegramUserModel.objects.get(name=uid)
    except:
        return 'شما ثبت نام نکرده اید. لطفا ابتدا ثبت نام کنید! ' + '/register'
    try:
        course = CourseModel.objects.get(id=user.course)
        lesson = course.lessonmodel_set.all().filter(lesson_number=user.last_lesson)[0]
        words = lesson.words.all()
    except:
        return 'شما درسی ثبت نام نکرده اید! ابتدا ثتب نام کنید! ' + '/register'
    try:
        header = create_header(lesson.name, course.name)
        text = create_lesson(words)
        footer = create_footer()
        return header + text + footer
    except Exception as e:
        return 'ERROR ' + e.message


@csrf_exempt
def send_message_view(request):
    global state
    if request.method == 'POST':
        print 'raw data', request.body
        body = dict(json.loads(request.body))

        update_id = body['update_id']
        message = body['message']
        message_id = message.get('message_id')
        date = message.get('date')
        text = message.get('text')
        fr = message.get('from')
        chat = message['chat']
        chat_id = chat['id']

        if not text:
            return HttpResponse("No Text")

        def reply(msg=None, reply_markup=None, reply_to_message_id=True):
            params = {
                'chat_id': str(chat_id),
                'text': msg.encode('utf-8'),
                'disable_web_page_preview': 'true',
            }
            if reply_markup:
                params['reply_markup'] = reply_markup
            if reply_to_message_id:
                params['reply_to_message_id'] = message_id
            params = urllib.urlencode(params)
            if msg:
                resp = urllib2.urlopen(BASE_URL + 'sendMessage', params).read()
            return resp

        if text.startswith('/'):
            if text == REGISTER_COMMAND:
                msg, reply_markup = register_course()
                reply('یکی از درس های زیر را انتخابکنید!', reply_markup)
                state = 'REGISTER'
            elif text == LESSON_COMMAND:
                msg = get_lesson(fr['id'])
                reply(msg, reply_to_message_id=False)
                state = 'LESSON'
            elif text == HELP_COMMAND:
                reply('Help')
                state = 'HELP'
            elif text == STOP_COMMAND:
                reply('Stop')
                state = 'STOP'
            elif text == '/del':
                reply('BOT: delete command')
                user = TelegramUserModel.objects.get(name=fr['id'])
                user.course = -1
                user.save()
                reply('deleted!')
                state = 'DELETE'
        elif state == 'REGISTER':
            if state == 'REGISTER' and text.strip() == COURSE_504.strip():
                try:
                    # get the course
                    course = CourseModel.objects.get_or_create(name=COURSE_504)[0]
                    # save the course
                    course.save()

                    if TelegramUserModel.objects.filter(name=fr['id']).exists():
                        # get the user
                        user = TelegramUserModel.objects.get(name=fr['id'])
                        # set the course id
                        user.course = course.id
                        # reset the lesson
                        user.last_lesson = 1
                        reply('درس جدید برای شما تنظیم شد!')

                    else:
                        # create a new user
                        user = TelegramUserModel.objects.create(name=fr['id'], course=course.id)
                        user.last_lesson = 1
                        reply('ثبت نام با موفقیت انجام شد!')
                    # save the results
                    user.save()
                except Exception as e:
                    state = ''
                    reply(e.message)
            else:
                reply('پاسخ شما نامربوط است! لطفا دوباره امتحان کنید')
        else:
            reply('چی شد ؟‌:)')
        return JsonResponse({'result': 'post'})
    else:
        return JsonResponse({'result': 'get'})