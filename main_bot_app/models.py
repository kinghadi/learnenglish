from django.db import models


class CourseModel(models.Model):
    name = models.CharField(max_length=50)
    description = models.CharField(max_length=150)
    date_created = models.DateTimeField(auto_created=True, null=True, blank=True)

    class Meta:
        ordering = ('date_created',)

    def __unicode__(self):
        return self.name


class ContributerModel(models.Model):
    name = models.CharField(max_length=100)
    description = models.CharField(max_length=150)
    courses = models.ManyToManyField('main_bot_app.CourseModel')
    date_created = models.DateTimeField(auto_created=True, null=True, blank=True)

    class Meta:
        ordering = ('date_created',)

    def __unicode__(self):
        return self.name


class LessonModel(models.Model):
    name = models.CharField(max_length=100)
    words = models.ManyToManyField('main_bot_app.WordModel')
    courses = models.ManyToManyField('main_bot_app.CourseModel')
    lesson_number = models.IntegerField(default=1)
    date_created = models.DateTimeField(auto_created=True, null=True, blank=True)

    class Meta:
        ordering = ('date_created',)


    def __unicode__(self):
        return self.name


class WordModel(models.Model):
    word = models.CharField(max_length=100)
    pronounce = models.CharField(max_length=150, null=True, blank=True)
    synonym_eng = models.TextField(null=True, blank=True)
    antonym_eng = models.TextField(null=True, blank=True)
    meaning_fa = models.TextField()
    examples = models.TextField()
    date_created = models.DateTimeField(auto_created=True, null=True, blank=True)

    class Meta:
        ordering = ('date_created',)

    def __unicode__(self):
        return self.word


class TelegramUserModel(models.Model):
    name = models.CharField(max_length=100)
    course = models.IntegerField(null=True, blank=True, default=-1)
    last_lesson = models.IntegerField(null=True, blank=True, default=-1)
    state = models.IntegerField(default=0)
    date_created = models.DateTimeField(auto_created=True, null=True, blank=True)

    class Meta:
        ordering = ('date_created',)

    def __unicode__(self):
        return self.name
