from django.contrib import admin
from main_bot_app.models import *

admin.site.register((CourseModel, ContributerModel, LessonModel, TelegramUserModel, WordModel))