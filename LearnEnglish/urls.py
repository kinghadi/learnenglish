from django.conf.urls import patterns, include, url
from django.contrib import admin
from django.views.generic import TemplateView
from django.views.static import serve
from main_bot_app.views import *
from django.conf import settings

urlpatterns = patterns('',
                       # Examples:
                       # url(r'^$', 'LearnEnglish.views.home', name='home'),
                       # url(r'^blog/', include('blog.urls')),
                       url(r'^$', TemplateView.as_view(template_name='main_page.html')),
                       url(r'^admin/', include(admin.site.urls)),
                       url(r'^getme/$', getme_view),
                       url(r'^update/$', getupdate_view),
                       url(r'^setwebhook/$', set_webhook_view),
                       url(r'^webhook$', send_message_view),
)

if settings.DEBUG:
    urlpatterns += [
        url(r'^media/(?P<path>.*)$', serve, {
            'document_root': settings.MEDIA_ROOT,
        }),
    ]